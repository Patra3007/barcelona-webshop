<?php
session_start();
if (!isset($_SESSION['email'])) {
    header("Location: login.php");
}
?>

<!DOCTYPE html>
<html lang="hr">

<head>
    <?php include('html-components/header.html'); ?>
</head>

<body>

    <?php include('html-components/navbar.php'); ?>

    <div class="main-page">
        <div class="main-page-container">
            <h1>Moja košarica</h1>
            <hr>
            <table>
                <tr>
                    <th width="30%">Naziv</th>
                    <th width="10%">Opis</th>
                    <th width="13%">Količina</th>
                    <th width="10%">Jedinična cijena</th>
                    <th width="20%">Cijena</th>
                    <th width="17%"></th>
                </tr>

                <?php
                if(!empty($_SESSION["cart"])){
                    $total = 0;
                    foreach ($_SESSION["cart"] as $key => $value) {
                        ?>
                <tr>
                    <td><?php echo $value["naziv"]; ?></td>
                    <td><?php echo $value["opis"]; ?></td>
                    <td><?php echo $value["kolicina"]; ?></td>
                    <td><?php echo number_format($value["cijena"], 2); ?> HRK</td>
                    <td>
                        <?php echo number_format($value["kolicina"] * $value["cijena"], 2); ?> HRK</td>
                    <td><a href="index.php?action=delete&id=<?php echo $value["id"]; ?>">
                            <span class="btn btn-danger">Ukloni</span></a></td>
                </tr>
                <?php
                        $total = $total + ($value["kolicina"] * $value["cijena"]);
                    }
                        ?>
                <tr>
                    <td colspan="3"></td>
                    <th colspan="2"><b>Ukupno: </b><?php echo number_format($total, 2); ?> HRK</th>
                    <th colspan="1"><a
                            href='mailto: patrik.juzbasic@net.hr?subject=Kupovina Barca proizvoda&body= <?php foreach ($_SESSION["cart"] as $key => $value){ ?>NAZIV: <?php echo $value["naziv"]; ?> OPIS: <?php echo $value["opis"]; ?>, KOLIČINA: <?php echo $value["kolicina"]; ?>, JEDINIČNA CIJENA: <?php echo $value["cijena"]?> HRK.<?php } ?> UKUPNO ZA POŠILJKU: <?php echo $total ?> EUR. U nastavku unesite Vašu adresu za slanje paketa. Plaćanje je isključivo pouzećem. Za pošiljke iznad 100,00 EUR dostava je BESPLATNA.'
                            target='_top'><button type='button' class='btn btn-success'>Naruči</button></a></th>
                    <td></td>
                </tr>
                <?php
                    }
                ?>
            </table>

        </div>

    </div>
</body>

</html>